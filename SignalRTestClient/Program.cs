﻿
using Common.Extensions;
using Microsoft.AspNetCore.SignalR.Client;
using SignalRTestClient;

Console.WriteLine("正在连接服务器...");

//假设当前用户一登录这个时候添加连接

var userId = "123456";
var projectId = "654321";
var hubconnection = new HubConnectionBuilder()
    .WithUrl("http://localhost:5012/testhub", (obj) =>
    {
        var keyObj = new SignalRClient
        {
            Exp = DateTime.Now.AddMinutes(5),
            Type = SignalRClientType.client,
            UserId = userId,
            ProjectId = projectId
        };
        obj.Headers.Add("access_token", keyObj.PublishSignalRConnectionKey());
    })
    .WithAutomaticReconnect()
    .Build();

//订阅showmsg方法
hubconnection.On<Msg>("showmsg", (msg) =>
{
    Console.WriteLine($"服务器推送消息：{msg.Context}");
});
hubconnection.Closed += (error) =>
{
    Console.WriteLine($"连接失败:{error?.Message}，退出！");
    return Task.CompletedTask;
};

try
{
    await hubconnection.StartAsync();
    //await hubconnection.InvokeAsync("BindClient", "123456", "654321");
    Console.WriteLine($"客户端ID: {hubconnection.ConnectionId} 已连接到服务器");
    Console.WriteLine($"客户端ID: {hubconnection.ConnectionId} 已绑定用户{userId}，加入群：{projectId}");
}
catch (Exception ex)
{
    Console.WriteLine($"客户端ID: {hubconnection.ConnectionId} 连接失败：{ex.Message}");
}

var code = await hubconnection.InvokeAsync<string>("GetCode");
Console.WriteLine($"获取到扫码code={code}");

Console.ReadLine();