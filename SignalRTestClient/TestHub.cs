﻿using Microsoft.AspNetCore.SignalR;

namespace SignalRTestClient
{
    public class TestHub : Hub
    {
    }

    public class Msg
    {
        public string Context { get; set; }
    }
}
