using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;

namespace ClientSendMessage.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class TestController : ControllerBase
    {
        private readonly HubConnection _testhubconnection;
        public TestController(HubConnection testhubconnection)
        {
            _testhubconnection = testhubconnection;
        }

        [HttpGet]
        public DateTime GetTime()
        {
            return DateTime.Now;
        }
        //这里是管理员客户端调用服务端SendMessage 方法项其他订阅端发送方法

        /// <summary>
        /// 和某用户私聊
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task SendOneMessage([FromQuery] string id, [FromQuery] string msg)
        {
            //如未连接成功则报错
            await _testhubconnection.InvokeAsync("SendOneMessage", id, msg);

        }

        /// <summary>
        /// 和某群群聊
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task SendGroupMessage([FromQuery] string group, [FromQuery] string msg)
        {
            await _testhubconnection.InvokeAsync("SendGroupMessage", group, msg);
        }

        /// <summary>
        /// 和所有人聊
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task SendAllMessage([FromQuery] string msg)
        {
            await _testhubconnection.InvokeAsync("SendAllMessage", msg);
        }
    }
}