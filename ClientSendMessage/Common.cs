﻿using Common.Extensions;
using Microsoft.AspNetCore.SignalR.Client;

namespace ClientSendMessage
{
    public static class Common
    {
        public static void ConfigPushSignalR(this IServiceCollection services)
        {

            //假设当前用户一登录这个时候添加连接


            //这里主要是向服务中心中的其他用户推送消息
            var hubconnection = new HubConnectionBuilder()
                .WithUrl("http://localhost:5012/testhub", (obj) =>
                {
                    var keyObj = new SignalRClient
                    {
                        Exp = DateTime.Now.AddMinutes(10),
                        Type = SignalRClientType.admin,
                        UserId = "收费端后台"
                    };
                    obj.Headers.Add("access_token", keyObj.PublishSignalRConnectionKey());
                })
                .WithAutomaticReconnect()
                .Build();

            services.AddSingleton(hubconnection);
        }

        public static void RegisterPushSignalR(this WebApplication app)
        {
            //hubconnection.On<Msg>("showmsg", (msg) => {
            //    Console.WriteLine($"服务器推送消息：{msg.Context}");
            //});

            var hubconnection = app.Services.GetRequiredService<HubConnection>();
            hubconnection.On<Msg>("showmsg", (msg) =>
            {
                Console.WriteLine($"服务器推送消息：{msg.Context}");
            });
            hubconnection.Closed += (error) =>
            {
                Console.WriteLine($"连接失败，退出！");
                return Task.CompletedTask;
            };
            app.Lifetime.ApplicationStarted.Register(() =>
            {
                try
                {
                    hubconnection.StartAsync().Wait();
                    Console.WriteLine($"管理员客户端ID: {hubconnection.ConnectionId} 已连接到服务器");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"管理员客户端ID: {hubconnection.ConnectionId} 连接异常：{e?.Message}");
                }

            });

            app.Lifetime.ApplicationStopped.Register(() =>
            {
                hubconnection.StopAsync().Wait();
                Console.WriteLine($"管理员客户端ID: {hubconnection.ConnectionId} 已退出服务器");
            });
        }
    }
}
