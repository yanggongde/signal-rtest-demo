﻿using Microsoft.AspNetCore.SignalR;

namespace ClientSendMessage
{
    public class TestHub : Hub
    {
    }

    public class Msg
    {
        public string Context { get; set; }
    }
}
