﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace Common.Extensions
{
    /// <summary>
    /// string 类型操作扩展类
    /// </summary>
    public static class StringExtension
    {
        


        /// <summary>
        /// 把yyyyMMddHHmmss格式时间字符串转换为时间
        /// </summary>
        /// <param name="timestr"></param>
        /// <returns></returns>
        public static DateTime ConvertFormatStrToDateTime(this string timestr)
        {
            try
            {
                return DateTime.ParseExact(timestr, "yyyyMMddHHmmss", System.Globalization.CultureInfo.CurrentCulture);
            }
            catch (Exception)
            {
                return default;
            }

        }

        /// <summary>
        /// 把format格式时间字符串转换为时间
        /// </summary>
        /// <param name="timestr"></param>
        /// <param name="format">时间格式化规则 例如：yyyyMMddHHmmss</param>
        /// <returns></returns>
        public static DateTime ConvertFormatStrToDateTime(this string timestr, string format)
        {
            try
            {
                return DateTime.ParseExact(timestr, format, System.Globalization.CultureInfo.CurrentCulture);
            }
            catch (Exception)
            {
                return default;
            }

        }


        /// <summary>  
        /// 反序列化xml字符为对象  
        /// </summary>  
        /// <typeparam name="T"></typeparam>  
        /// <param name="xml"></param>   
        /// <returns></returns>  
        public static T XMLSerialize<T>(this string xml) where T : new()
        {
            try
            {
                xml = Regex.Replace(xml, @"<\?xml*.*?>", "", RegexOptions.IgnoreCase);
                XmlSerializer xmlSer = new XmlSerializer(typeof(T), new XmlRootAttribute("xml"));
                using (StringReader xmlReader = new StringReader(xml))
                {
                    return (T)xmlSer.Deserialize(xmlReader);
                }
            }
            catch
            {
                return default(T);
            }
        }


        /// <summary>
        /// object to string
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string M5_StrToString(this object str)
        {
            if (str == null)
            {
                return "";
            }
            return str.ToString();
        }

        /// <summary>
        ///  验证是否为空
        /// </summary>
        /// <param name="str"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static string M5_StrToIsnull(this string str, string message = "字符串不能为空")
        {
            if (string.IsNullOrEmpty(str))
            {
                throw new ArgumentNullException(message);
            }
            return str;
        }

        /// <summary>
        /// 字符串去除空格
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string M5_StrTrim(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }
            return str.Trim();
        }

        /// <summary>
        /// 获取文件的绝对路径
        /// </summary>
        /// <param name="path">文件相对路径</param>
        /// <param name="domain">文件域</param>
        /// <returns></returns>
        public static string M5_FileAbsolutePath(this string path, string domain)
        {
            if (string.IsNullOrEmpty(path))
            {
                return string.Empty;
            }
            return Path.Combine(domain, path);
        }

        /// <summary>
        /// 返回MD5加密后的字符串
        /// </summary>
        /// <param name="encryptString">原始字符串</param>
        /// <returns>MD5加密字符串</returns>
        public static string M5_StringToMd5(this string encryptString)
        {
            byte[] b = Encoding.Default.GetBytes(encryptString);
            b = new MD5CryptoServiceProvider().ComputeHash(b);

            string ret = "";
            for (int i = 0; i < b.Length; i++)
                ret += b[i].ToString("x").PadLeft(2, '0');

            return ret;
        }

        /// <summary>
        /// 返回MD5加密后的字符串
        /// </summary>
        /// <param name="encryptStream">原始字符串</param>
        /// <returns>MD5加密字符串</returns>
        public static string M5_StreamToMd5(this Stream encryptStream)
        {
            byte[] b = new MD5CryptoServiceProvider().ComputeHash(encryptStream);

            string ret = "";
            for (int i = 0; i < b.Length; i++)
                ret += b[i].ToString("x").PadLeft(2, '0');

            return ret;
        }

        /// <summary>
        /// 返回MD5加密后的字符串
        /// </summary>
        /// <param name="encryptString">原始字符串</param>
        /// <param name="ecode">编码方式</param>
        /// <returns></returns>
        public static string M5_StringToMd5(this string encryptString, Encoding ecode)
        {
            byte[] b = ecode.GetBytes(encryptString);
            b = new MD5CryptoServiceProvider().ComputeHash(b);

            string ret = "";
            for (int i = 0; i < b.Length; i++)
                ret += b[i].ToString("x").PadLeft(2, '0');

            return ret;
        }


        private static byte[] Keys = { 0x13, 0x35, 0x57, 0x79, 0x91, 0xAC, 0xDC, 0xFE };

        /// <summary>
        /// DES加密字符串
        /// </summary>
        /// <param name="encryptString">待加密的字符串</param>
        /// <param name="encryptKey">加密密钥,要求为8位</param>
        /// <returns>加密成功返回加密后的字符串，失败返回源串</returns>        
        public static string M5_DESEncrypt(this string encryptString, string encryptKey="4g@3l&98Ko%Sx")
        {
            if (string.IsNullOrEmpty(encryptString) || string.IsNullOrEmpty(encryptKey))
            {
                return string.Empty;
            }
            MemoryStream mStream = new MemoryStream();
            try
            {
                byte[] rgbKey = Encoding.UTF8.GetBytes(encryptKey.Substring(0, 8));
                byte[] rgbIV = Keys;
                byte[] inputByteArray = Encoding.UTF8.GetBytes(encryptString);
                DESCryptoServiceProvider dCSP = new DESCryptoServiceProvider();

                CryptoStream cStream = new CryptoStream(mStream, dCSP.CreateEncryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                return Convert.ToBase64String(mStream.ToArray());
            }
            catch
            {
                return string.Empty;
            }
            finally
            {
                mStream.Close();
            }
        }
        /// <summary>
        /// DES解密字符串
        /// </summary>
        /// <param name="decryptString">待解密的字符串</param>
        /// <param name="decryptKey">解密密钥,要求为8位,和加密密钥相同</param>
        /// <returns>解密成功返回解密后的字符串，失败返源串</returns>
        public static string M5_DESDecrypt(this string decryptString, string decryptKey= "4g@3l&98Ko%Sx")
        {

            if (string.IsNullOrEmpty(decryptString) || string.IsNullOrEmpty(decryptKey))
            {
                return string.Empty;
            }
            MemoryStream mStream = new MemoryStream();
            try
            {
                byte[] rgbKey = Encoding.UTF8.GetBytes(decryptKey.Substring(0, 8));
                byte[] rgbIV = Keys;
                byte[] inputByteArray = Convert.FromBase64String(decryptString);
                DESCryptoServiceProvider DCSP = new DESCryptoServiceProvider();

                CryptoStream cStream = new CryptoStream(mStream, DCSP.CreateDecryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                return Encoding.UTF8.GetString(mStream.ToArray());
            }
            catch
            {
                return string.Empty;
            }
            finally
            {
                mStream.Close();
            }
        }


        /// <summary>
        ///  AES 加密
        /// </summary>
        /// <param name="str">明文（待加密）</param>
        /// <param name="key">密文 32位</param>
        /// <param name="iv">辅助向量 16位</param>
        /// <returns></returns>
        public static string M5_AESEncrypt(this string str, string key, string iv)
        {
            if (string.IsNullOrEmpty(str)) return null;

            try
            {
                Byte[] toEncryptArray = Encoding.UTF8.GetBytes(str);

                RijndaelManaged rm = new RijndaelManaged
                {
                    IV = Encoding.UTF8.GetBytes(iv),
                    Key = Encoding.UTF8.GetBytes(key),
                    Mode = CipherMode.CBC,
                    Padding = PaddingMode.PKCS7
                };

                ICryptoTransform cTransform = rm.CreateEncryptor();
                Byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                return Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
            catch (Exception)
            {
                return null;
            }

        }

        /// <summary>
        ///  AES 解密
        /// </summary>
        /// <param name="str">明文（待解密）</param>
        /// <param name="key">密文 32位</param>
        /// <param name="iv">辅助向量 16位</param>
        /// <returns></returns>
        public static string M5_AESDecrypt(this string str, string key, string iv)
        {
            if (string.IsNullOrEmpty(str)) return null;
            try
            {
                Byte[] toEncryptArray = Convert.FromBase64String(str);

                RijndaelManaged rm = new RijndaelManaged
                {
                    IV = Encoding.UTF8.GetBytes(iv),
                    Key = Encoding.UTF8.GetBytes(key),
                    Mode = CipherMode.CBC,
                    Padding = PaddingMode.PKCS7
                };

                ICryptoTransform cTransform = rm.CreateDecryptor();
                Byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                return Encoding.UTF8.GetString(resultArray);
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}
