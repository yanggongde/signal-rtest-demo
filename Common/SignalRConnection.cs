﻿
namespace Common.Extensions
{
    public static class SignalRConnection
    {
        public static string PublishSignalRConnectionKey(this SignalRClient input)
        {
            return input.M5_ObjectToJson().M5_DESEncrypt();
        }

        public static SignalRClient CheckSignalRConnectionKey(this string key)
        {
            if (string.IsNullOrEmpty(key)) return null;
            try
            {
                return key.M5_DESDecrypt()?.M5_JsonToObject<SignalRClient>();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    /// <summary>
    /// 用这个验证可以通用，如果用jwt token验证的话每个端的规则都不一样
    /// </summary>
    public class SignalRClient
    {
        public SignalRClientType Type { get; set; }
        public string UserId { get; set; }
        public DateTime Exp { get; set; }
        public string ProjectId { get; set; }
    }

    public enum SignalRClientType
    {
        admin,
        client
    }
}