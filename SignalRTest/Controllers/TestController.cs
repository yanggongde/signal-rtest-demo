using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using SignalRTest.CM;

namespace SignalRTest.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class TestController : ControllerBase
    {

        private readonly ILogger<TestController> _logger;
        private readonly IHubContext<TestHub> _testHubContext;

        public TestController(ILogger<TestController> logger, IHubContext<TestHub> testHubContext)
        {
            _logger = logger;
            _testHubContext = testHubContext;
        }

        /// <summary>
        /// 所有人发
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task SendAllMessage(string message)
        {
            //_testHubContext.Groups
            await _testHubContext.Clients.All.SendAsync("showmsg", new Msg { Context = message });
        }

        /// <summary>
        /// 多组群发
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task SendGroupMessage([FromQuery] string message)
        {
            var groups = new List<string> { TestHub.DefaultGroup };
            await _testHubContext.Clients.Groups(groups).SendAsync("showmsg", new Msg { Context = $"定向组{TestHub.DefaultGroup}：{message}" });
            //await _testHubContext.Clients.Group(TestHub.DefaultGroup).SendAsync("showmsg", new Msg { Context = $"定向组{TestHub.DefaultGroup}：{message}" });
        }

        /// <summary>
        /// 单用户发
        /// </summary>
        /// <param name="connectionID"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task SendOneMessage([FromQuery]string connectionID, [FromQuery] string message)
        {
            await _testHubContext.Clients.Client(connectionID).SendAsync("showmsg", new Msg { Context = $"定向用户{connectionID}：{message}" });
        }



        /// <summary>
        /// 多用户群发
        /// </summary>
        /// <param name="connectionIDs"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task SendMoreMessage([FromQuery] List<string> connectionIDs, [FromQuery] string message)
        {
            await _testHubContext.Clients.Clients(connectionIDs).SendAsync("showmsg", new Msg { Context = $"定向用户{string.Join(',', connectionIDs)}：{message}" });
        }
    }
}