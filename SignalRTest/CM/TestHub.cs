﻿using Common.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Memory;

namespace SignalRTest.CM
{
    //也可以用jwt验证 请求头上传
    //[Authorize] 
    public class TestHub : Hub
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public const string DefaultGroup = "TestGroup";

        private IMemoryCache _cache;
        public TestHub(IMemoryCache cache, IHttpContextAccessor httpContextAccessor)
        {
            _cache = cache;
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 这里身份连接的时候验证就可以了
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public override async Task OnConnectedAsync()
        {
            string token = _httpContextAccessor.HttpContext.Request.Headers["access_token"];
            // 实际校验过程根据个人项目决定，成功则连接，不成功直接返回
            if (string.IsNullOrEmpty(token))
            {
                Context.Abort();
                throw new Exception("无效身份连接");
            }

            //用这个验证可以通用，如果用jwt token验证的话每个端的规则都不一样
            var clientObj = token.CheckSignalRConnectionKey();
            if (clientObj == null)
            {
                Context.Abort();
                throw new Exception("无效身份连接");
            }
            //管理不绑定关系不接受消息，只发送，客户端只订阅消息 不发送
            if (clientObj.Type == SignalRClientType.client)
            {
                if (clientObj.Exp < DateTime.Now)
                {
                    Context.Abort();
                    throw new Exception("无效身份连接");
                }

                //绑定ConnectionId和clientObj.UserId的关系
                //可以根据不同的组【可以理解为易隆物业的一个项目一个组】
                await Groups.AddToGroupAsync(Context.ConnectionId, clientObj.ProjectId);
            }

            Console.WriteLine($"【{Context.ConnectionId}】客户端：【{clientObj.Type.ToString()}】 【{clientObj.UserId}】已连接");

            //待分配客户端的连接ID【可以理解为易隆物业的一个用户】
            //var connectionId = Context.ConnectionId;
            //这里可以用当前用户的Id和这个连接ID匹配,便于在业务中区别连接用户，标识用户已上线
            //例如可以根据用户id查找当前有哪些正在连接的客户端，推送消息

            await base.OnConnectedAsync();
        }

        [AllowAnonymous]
        public override async Task OnDisconnectedAsync(Exception? exception)
        {
            Console.WriteLine($"【{Context.ConnectionId}】客户端：已退出！");
            //断开从群中删除,根据连接id找到组，删除关系
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, "group");
            //这里删除用户和连接之间的关联，标识用户已下线
            await base.OnDisconnectedAsync(exception);
        }

        /// <summary>
        /// 定义便于管理员客户端调用
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<string> GetCode()
        {
            //Context.ConnectionId
            return await Task.Run(() =>
            {
                return Guid.NewGuid().ToString();
            });
        }

        /// <summary>
        /// 定义便于管理员客户端调用
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task SendOneMessage(string id, string msg)
        {
            await Clients.Client(id).SendAsync("showmsg", new Msg { Context = $"管理员【{Context.ConnectionId}】端定向私聊：{id}：{msg}" }); ;
        }

        /// <summary>
        /// 定义便于管理员客户端调用
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task SendGroupMessage(string group, string msg)
        {
            await Clients.Group(group).SendAsync("showmsg", new Msg { Context = $"管理员【{Context.ConnectionId}】端定向群：{group}群聊：{msg}" }); ;
        }

        /// <summary>
        /// 定义便于管理员客户端调用
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task SendAllMessage(string msg)
        {
            await Clients.All.SendAsync("showmsg", new Msg { Context = $"管理员【{Context.ConnectionId}】端所有推送公告：{msg}" }); ;
        }

        ///// <summary>
        ///// 定义便于订阅段调用
        ///// </summary>
        ///// <param name="userid"></param>
        ///// <param name="projectid"></param>
        ///// <returns></returns>
        //public async Task BindClient(string userid, string projectid)
        //{
        //    //Context.ConnectionId和userid绑定关系这里是用文字描述
        //    //Context.ConnectionId和组绑定关系
        //    await Groups.AddToGroupAsync(Context.ConnectionId, projectid);
        //}
    }

    public class Msg
    {
        public string Context { get; set; }
    }
}
