﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Collections.Generic;
using System;
using Microsoft.AspNetCore.Http;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Controllers;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;

namespace SignalRCenter
{
    public class GlobalResultFilterAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            base.OnResultExecuting(context);
        }

    }

    /// <summary>
    /// API请求后续校验
    /// 校验顺序 JWT -> 后续校验【单点校验 ->权限校验】
    /// </summary>
    public class GlobalValidateOAuthFilter : IAsyncActionFilter, IOrderedFilter
    {
        /// <summary>
        /// 请求上下文
        /// </summary>
        private readonly IHttpContextAccessor _httpContextAccessor;

        public int Order => 1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="redis"></param>
        /// <param name="httpContextAccessor"></param>
        public GlobalValidateOAuthFilter(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var actionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            var method = actionDescriptor.MethodInfo;


            //开启授权校验
            if (method.IsDefined(typeof(AuthorizeAttribute)))
            {
                var isAllowAnonymous = method.IsDefined(typeof(AllowAnonymousAttribute));

            }

            //模型验证
            ValidateModel(context);

            await next();
        }

        private void ValidateModel(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                foreach (var state in context.ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        throw new Exception(error.ErrorMessage);
                    }
                }
            }
        }

    }

    /// <summary>
    /// 标注跳过权限校验（注意不会跳过JWT校验，jwt跳过AllowAnonymousAttribute标注）
    /// 跳过JWT+权限校验标注 AllowAnonymousAttribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class SkipPermissionCheckAttribute : Attribute
    {

    }
}
