using Microsoft.AspNetCore.SignalR;
using SignalRCenter;
using SignalRCenter.CM;
using SignalRTest.CM;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers(options =>
{
    options.Filters.Add<GlobalValidateOAuthFilter>();
    options.Filters.Add<GlobalResultFilterAttribute>();
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSignalR(options =>
{
    options.EnableDetailedErrors = true;
    //options.AddFilter<CusHubFilter>();
});
builder.Services.AddHttpContextAccessor();
//memoryCache   
builder.Services.AddMemoryCache();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


app.UseRouting();
//app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();
//app.UseJwtSignalRAuthentication();
app.UseEndpoints(endpoints =>
{
    endpoints.MapHub<TestHub>("testhub");
});
app.Run();
