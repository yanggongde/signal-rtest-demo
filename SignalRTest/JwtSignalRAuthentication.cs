﻿namespace SignalRCenter
{
    public static class JwtSignalRExtensions
    {
        private const string ACCESS_TOKEN_STRING_KEY = "access_token";

        public static void UseJwtSignalRAuthentication(this IApplicationBuilder app)
        {
            app.Use(async (context, next) =>
            {
                if (string.IsNullOrWhiteSpace(context.Request.Headers["Authorization"]))
                {
                    try
                    {
                        if (context.Request.QueryString.HasValue)
                        {
                            var token = context.Request.Query[ACCESS_TOKEN_STRING_KEY];
                            if (!string.IsNullOrWhiteSpace(token))
                            {
                                context.Request.Headers.Add("Authorization", new[] { $"Bearer {token}" });
                            }
                        }
                    }
                    catch
                    {
                    }
                }
                await next.Invoke();
            });
        }
    }
}
